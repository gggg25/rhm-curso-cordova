 var app = angular.module('cordova', ["ngRoute"]);
  app.controller('apacheCtr', ['$scope', function($scope) {
    $scope.title = "llego a bootstrap ";
  }]);

  app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
      // controller : "inincioCtr",
      templateUrl: 'vistas/inicio.html'
    })
    .when("/bootstrap", {
      controller : "apacheCtr",
      templateUrl: 'vistas/bootstrap.html'
    });
  });
  angular.bootstrap(document, ['cordova']);