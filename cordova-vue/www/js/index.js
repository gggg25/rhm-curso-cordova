/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
		/*veremos si esto es con o sin en el primer ejercicio
		verrrrr
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
		*/
        console.log('Received Event: ' + id);

		// www/js/index.js
		/*primero
		var viewApache = {
			  title: 'Apache Cordova Framework'
			};
			var viewVue = {
			  title: 'Vue.js Framework'
			};
			var viewMobile= {
			  title: 'Mobile Development'
			};

			var app = new Vue({
			  el: '#app',
			  data: {
				viewApache: viewApache,
				viewVue: viewVue,
				viewMobile: viewMobile
			  }
			});
	    */
		/* segundo */
		  // var viewApache = {
			//   title: 'Apache Cordova Framework'
			// };
			// var viewVue = {
			//   title: 'Vue.js Framework'
			// };
			// var viewMobile= {
			//   title: 'Mobile Development'
			// };

			/*varian las app en los js tener en cuenta eso en los js
			var app = new Vue({
			  el: '#app',
			  data: {
				currentView: viewApache,
				viewApache: viewApache,
				viewVue: viewVue,
				viewMobile: viewMobile
			  }
			});
			*/
			//****botones
			// var nextView = function(view) {
			//   switch (view) {
			// 	case viewApache: this.currentView = viewVue; break;
			// 	case viewVue: this.currentView = viewMobile; break;
			//   }
			//   console.log('next view', this.currentView.title);
			// };
			// var prevView = function(view) {
			//   switch (view) {
			// 	case viewVue: this.currentView = viewApache; break;
			// 	case viewMobile: this.currentView = viewVue; break;
			//   }
			//   console.log('prev view', this.currentView.title);
			// };

			// var app = new Vue({
			//   el: '#app',
			//   data: {
			// 	currentView: viewApache,
			// 	viewApache: viewApache,
			// 	viewVue: viewVue,
			// 	viewMobile: viewMobile
			//   },
			//   methods: {
			// 	next: nextView,
			// 	prev: prevView
			//   }
			// });

		//

    // definimos la ruta
    // Creamos los componentes para los templates
    var viewApache = Vue.component('view-apache', {
        template: '#view-apache',
        data: function () {
          return { message: 'Apache Cordova Framework' }
        }
      });
      var viewVue = Vue.component('view-vue', {
        template: '#view-vue',
        data: function () {
          return { message: 'Vue.js Framework' }
        }
      });
      var viewMobile = Vue.component('view-mobile', {
        template: '#view-mobile',
        data: function () {
          return { message: 'Mobile Development' }
        }
      });

    // Definimos el router
    var router = new VueRouter({
      routes: [
        { path: '/apache', component: viewApache },
        { path: '/vue', component: viewVue },
        { path: '/mobile', component: viewMobile }
      ]
    });
    var vm = new Vue({
      el: '#app',
      data: {
        currentRoute: window.location.pathname
      },
      router: router
    });

    // *****

	}


};

app.initialize();
