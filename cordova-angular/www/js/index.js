/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
		/*
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
		*/
        console.log('Received Event: ' + id);
		
		
		/* contenido del scripts Agregamos 3 views
		
		var viewApache = {
		  title: 'Apache Cordova Framework'
		};
		var viewAngular = {
		  title: 'Vue.js Framework'
		};
		var viewMobile= {
		  title: 'Mobile Development'
		};

		function cordovaCtr($scope) {
		  $scope.viewApache = viewApache;
		  $scope.viewAngular = viewAngular;
		  $scope.viewMobile = viewMobile;
		}
		/*inicializa el angularJs*/
		/*angular.module('cordova', [])
			.controller('cordovaCtr', ['$scope', cordovaCtr]);
		angular.bootstrap(document, ['cordova']);*/
		
		/* *************************** */
		
		/* contenido del scripts Agregamos view actual
		
		var viewApache = {
		  title: 'Apache Cordova Framework'
		};
		var viewAngular = {
		  title: 'Vue.js Framework'
		};
		var viewMobile= {
		  title: 'Mobile Development'
		};

		function cordovaCtr($scope) {
		  $scope.currentView = viewApache;

		  $scope.viewApache = viewApache;
		  $scope.viewAngular = viewAngular;
		  $scope.viewMobile = viewMobile;
		}

		/*inicializa el angularJs*/
		/*angular.module('cordova', [])
			.controller('cordovaCtr', ['$scope', cordovaCtr]);
		angular.bootstrap(document, ['cordova']);
		
		/* *************************** */
		
		/* Agregamos botones y eventos 
		
		var viewApache = {
		  title: 'Apache Cordova Framework'
		};
		var viewAngular = {
		  title: 'Vue.js Framework'
		};
		var viewMobile= {
		  title: 'Mobile Development'
		};
		
		function cordovaCtr($scope) {
		  $scope.currentView = viewApache;
		  $scope.viewApache = viewApache;
		  $scope.viewAngular = viewAngular;
		  $scope.viewMobile = viewMobile;
		
		  $scope.nextView = function() {
			switch ($scope.currentView) {
			  case viewApache: $scope.currentView = viewAngular; break;
			  case viewAngular: $scope.currentView = viewMobile; break;
			}
			console.log('next view', $scope.currentView.title);
		  };
		  $scope.prevView = function() {
			switch ($scope.currentView) {
			  case viewAngular: $scope.currentView = viewApache; break;
			  case viewMobile: $scope.currentView = viewAngular; break;
			}
			console.log('prev view', $scope.currentView.title);
		  };
		}
		
		/*inicializa el angularJs
		angular.module('cordova', [])
			.controller('cordovaCtr', ['$scope', cordovaCtr]);
		angular.bootstrap(document, ['cordova']);
		/* *************************** */
		
		/* definimos el router */
		var app = angular.module('cordova', ["ngRoute"]);
		app.controller('apacheCtr', ['$scope', function($scope) {
		  $scope.title = "Apache Cordova Framework";
		}]);
		app.controller('angularCtr', ['$scope', function($scope) {
		  $scope.title = "AngularJS Framework";
		}]);
		app.controller('mobileCtr', ['$scope', function($scope) {
		  $scope.title = "Mobile Development";
		}]);
		
		app.config(function($routeProvider) {
		  $routeProvider
			.when("/apache", {
			  controller : "apacheCtr",
			  templateUrl: 'template.html'
			})
			.when("/angular", {
			  controller : "angularCtr",
			  templateUrl: 'template.html'
			})
			.when("/mobile", {
			  controller : "mobileCtr",
			  templateUrl: 'template.html'
			});
		});
		
		/*
		angular.module('cordova', [])
			.controller('cordovaCtr', ['$scope', cordovaCtr]);
			*/
		angular.bootstrap(document, ['cordova']);
		/* ********************* */
		
    }
};

app.initialize();